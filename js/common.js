﻿$(function(){
	//ie 버젼체크
    function get_version_of_IE () {

    	 var word;
    	 var version = "N/A";

    	 var agent = navigator.userAgent.toLowerCase();
    	 var name = navigator.appName;

    	 // IE old version ( IE 10 or Lower )
    	 if ( name == "Microsoft Internet Explorer" ) word = "msie ";

    	 else {
    		 // IE 11
    		 if ( agent.search("trident") > -1 ) word = "trident/.*rv:";

    		 // Microsoft Edge
    		 else if ( agent.search("edge/") > -1 ) word = "edge/";
    	 }

    	 var reg = new RegExp( word + "([0-9]{1,})(\\.{0,}[0-9]{0,1})" );

    	 if (  reg.exec( agent ) != null  ) version = RegExp.$1 + RegExp.$2;

    	 return version;
    }
    //ie 버젼 변수
    var verNumber = parseInt ( get_version_of_IE() , 10 );

	// Gnb
	$(".gnb_menu > ul > li").bind("mouseenter focusin", function() {
		$('.gnb').addClass('on');
		$('.gnb').stop().animate({
			height:'320px'
		},300);
	});
	$(".gnb_menu > ul > li").bind("mouseleave focusout", function() {
		$('.gnb').stop().animate({
			height:'60px'
		},300, function(){
			$('.gnb').removeClass('on');
		});
	});

	// recruit
	var reC = $('.info_recruit > ul > li');
	$(reC).each(function(i){
		$(this).bind("click focusin", function(e){
			e.preventDefault();
			reC.removeClass('on');
			$(this).addClass('on');
		});
	});

    //메인 효과
    if (document.getElementById('recruitView')) {
        //모집공고 버튼
    	$('#recruitList').find('a').each(function() {
    		$(this).on('click',function(e) {
    			e.preventDefault();
    			$(this).parent().addClass('js_on').siblings().removeClass('js_on');
    			$('#'+this.href.split('#')[1]).addClass('js_on').siblings().removeClass('js_on');
    		});
    	});
    } else {
	     //메인 마우스오버 효과
        $('.js_item_hover').on('mouseenter',function(){
            $(this).find('.main_menu').css('display','none');
            $(this).find('.main_column').css({'display':'block','opacity':'0'});
            $(this).css({'background-color':'#fff','border':'1px solid #c5c5c5'}).stop().animate({'height':'740px','padding':'40px 20px'},300,function(){
                $(this).find('.main_column').animate({'opacity':'1'},100)
            });
        }).on('mouseleave',function(){
            if (document.getElementById('recruitView')) return false;
            $(this).find('.main_menu').css('display','block');
            $(this).find('.main_column').css({'display':'none'});
            if(verNumber == 8){
                $(this).css({'background-color':'rgb(0,0,0)','border':'none'}).stop().animate({'height':'230px','padding':'0px'},300);
            }else{
                $(this).css({'background-color':'rgba(0,0,0,0.6)','border':'none'}).stop().animate({'height':'230px','padding':'0px'},300);
            }
        })
    }
});

//family site
$(function(){
	var el = $('#familySite');
	var btn = $(el.selector+'>h2');
	var list = $(el.selector+'>ul');

	$(btn).attr('title','목록 열기')

	$(btn).on('click keyup',function(e){
		if (e.type == "click" || e.keyCode == 13) {
			if ($(list).hasClass('on')) {
				$(list).stop().animate({top:'0'},100,function(){$(list).removeClass('on');});
				$(this).attr('title','목록 열기');
			} else {
				$(list).stop().animate({top:'40px'},100,function(){$(list).addClass('on');});
				$(this).attr('title','목록 닫기');
			}
		}
	})
});
