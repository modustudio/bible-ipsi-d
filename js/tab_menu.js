$(document).ready(function(){
	var idx = 0;
	var baseUrl = window.location.href.split('#')[0];
    var hash = window.location.hash;

    if (hash) {
        idx = /\d+/.exec(hash)[0];
        idx = (parseInt(idx) || 1) ;
		$('.major-list>li').removeClass('on').eq(idx-1).addClass('on');
		$('.major-area .contab').removeClass('on').eq(idx-1).addClass('on');
		console.log(idx);
    } else {
		window.location.replace(baseUrl + '#'+ 1);
		$('.major-list>li').removeClass('on').eq(0).addClass('on');
		$('.major-area .contab').removeClass('on').eq(0).addClass('on');
	}


	$('.major-list>li').each(function (index) {
		$(this).find('a').on('click', function(e){
			window.location.replace( baseUrl + '#' + (index+1) );
			e.preventDefault();
			$('.major-list>li').removeClass('on').eq(index).addClass('on');
			$(".major-area .contab").removeClass('on').eq(index).addClass('on');
		});
	});
});
